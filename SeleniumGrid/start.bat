@echo off

rem versão do SeleniumGrid
set urlSeleniumGrid=https://selenium-release.storage.googleapis.com/3.8/selenium-server-standalone-3.8.1.jar
set jarSeleniumGrid=selenium-server-standalone-3.8.1.jar
rem versão do IEDriverServer
set zipIEDriverServer=IEDriverServer_Win32_2.53.1.zip
set urlIEDriverServer=https://selenium-release.storage.googleapis.com/2.53/IEDriverServer_Win32_2.53.1.zip
set exeIEDriverServer=IEDriverServer.exe
rem versão do ChromeDriver
set zipChromeDriver=chromedriver_win32.zip
set urlChromeDriver=https://chromedriver.storage.googleapis.com/2.36/chromedriver_win32.zip
set exeChromeDriver=chromedriver.exe
rem versão do GeckoDriver
rem set zipGeckoDriver=geckodriver-v0.20.0-win64.zip
rem set urlGeckoDriver=https://github.com/mozilla/geckodriver/releases/download/v0.20.0/geckodriver-v0.20.0-win64.zip
rem set exeGeckoDriver=geckodriver.exe

cd Config

if not exist %jarSeleniumGrid% call startDownloadFile.bat %urlSeleniumGrid%
rem download IEDriverServer
if not exist %exeIEDriverServer% (
    call startDownloadFile.bat %urlIEDriverServer%
    call startUnpackZip.bat %zipIEDriverServer% %CD%
    del %zipIEDriverServer%
)
rem download ChromeDriver
if not exist %exeChromeDriver% (
    call startDownloadFile.bat %urlChromeDriver%
    call startUnpackZip.bat %zipChromeDriver% %CD%
    del %zipChromeDriver%
)

rem download GeckoDriver
rem if not exist %exeGeckoDriver% (
rem     call startDownloadFile.bat %urlGeckoDriver% 
rem     call startUnpackZip.bat %zipGeckoDriver% %CD%
rem     del %zipGeckoDriver%
rem )

call startHub.bat
call startNode.bat
exit