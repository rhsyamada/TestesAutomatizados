package br.com.automation.configuration.cucumber.hooks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.automation.configuration.spring.beans.ConfigWeb;
import br.com.automation.configuration.spring.beans.RepositoryTest;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	private static final String URL_PARAMETERS = "parameters/DefaultParameters";

	@Autowired
	ConfigWeb config;

	@Autowired
	RepositoryTest repo;

	@Before
	public void initialize(Scenario scenario) throws Throwable {
		ResourceBundle bundle = ResourceBundle.getBundle(URL_PARAMETERS);
		ArrayList<String> parms = Collections.list(bundle.getKeys());
		parms.forEach(p -> repo.setCollection(p, bundle.getString(p)));
	}

	@After
	public void finalize(Scenario scenario) throws Throwable {
		config.destroyer();
	}
}