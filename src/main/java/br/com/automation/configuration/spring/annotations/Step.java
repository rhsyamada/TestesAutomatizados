package br.com.automation.configuration.spring.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.automation.configuration.spring.componentscan.Application;

@Retention(RUNTIME)
@ContextConfiguration(classes = Application.class)
@RunWith(SpringRunner.class)
@SpringBootTest
@Order(value=2)
public @interface Step {

}
