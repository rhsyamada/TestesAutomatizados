package br.com.automation.configuration.spring.beans;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import br.com.automation.configuration.selenium.driver.RemoteWebDriverCustom;

@Configuration
@Order(value = 1)
public class ConfigWeb {
	private static final String URL_PARAMETERS = "parameters/DefaultParameters";
	private WebDriver driver;

	public void destroyer() {
		driver.close();
		driver.quit();
		driver = null;
	}

	public boolean isFilledWebDriver() {
		return driver != null;
	}

	private WebDriver createWebDriver() {
		try {
			ResourceBundle bundle = ResourceBundle.getBundle(URL_PARAMETERS);
			String urlGrid = bundle.getString("remotewebdriver.url");
			Integer timeoutGrid = Integer.parseInt(bundle.getString("remotewebdriver.timeout"));

			URL url = new URL(urlGrid);
			WebDriver driver = new RemoteWebDriverCustom(new RemoteWebDriver(url, getCapabilities()),
					timeoutGrid);
			driver.manage().window().maximize();
			return driver;
		} catch (Exception e) {
			throw new RuntimeException("Erro ao criar o webdriver.", e);
		}
	}
	
	private DesiredCapabilities getCapabilities() {
		String downloadFilepath = "/downloads";
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		return cap;
	}

	@Bean
	public WebDriver webDriver() {
		if (driver == null) {
			driver = createWebDriver();
		}
		return driver;
	}
}
