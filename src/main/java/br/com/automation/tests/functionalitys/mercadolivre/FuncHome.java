package br.com.automation.tests.functionalitys.mercadolivre;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.automation.configuration.spring.annotations.Functionality;
import br.com.automation.tests.functionalitys.base.FunctionalityBase;
import br.com.automation.tests.pageobejcts.mercadolivre.PageHome;

@Functionality
public class FuncHome extends FunctionalityBase {
	@Autowired
	private PageHome home;
	
	public void abrirSite() {
		home.openSite();
	}
	
	public void preencherBusca(String pesquisa) {
		home.setInputPesquisa(pesquisa);
		home.clickButtonPequisa();
	}
}
