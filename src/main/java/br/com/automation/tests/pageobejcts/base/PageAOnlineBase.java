package br.com.automation.tests.pageobejcts.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.automation.configuration.selenium.command.Command;

public class PageAOnlineBase extends PageBase {
	@Autowired
	private WebDriver driver;

	@Autowired
	public PageAOnlineBase(WebDriver driver) {
		super(driver);
	}

	public void navigateMenu(String... strings) {
		String xpath = "//ul[@id='CSSMENU']//*[text()='%s']";
		String xpathSeg = "//parent::li[1]//*[text()='%s']";

		StringBuilder str = new StringBuilder();

		for (int i = 0; i < strings.length; i++) {
			String aux = i == 0 ? xpath : xpathSeg;
			str.append(String.format(aux, strings[i]));

			WebElement element = driver.findElement(By.xpath(str.toString()));

			if (i == strings.length - 1)
				element.click();
			else {
				Command cmd = ((Command) driver);
				cmd.moveToElement(element);
			}
		}
	}
	
	
}
