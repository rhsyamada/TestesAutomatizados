package br.com.automation.tests.pageobejcts.base;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import br.com.automation.configuration.selenium.elementlocator.ElementLocatorFactoryCustom;

public class PageBase {
	private Map<String, Object[]> dynamicElementKeys = new HashMap<>();
	
	public PageBase(WebDriver driver) {
		PageFactory.initElements(new ElementLocatorFactoryCustom(driver, this), this);
	}
	
	protected void addDynamicElementKeys(String key, Object...value) {
		dynamicElementKeys.put(key, value);
	}
}
