package br.com.automation.tests.pageobejcts.mercadolivre;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.automation.configuration.spring.annotations.Page;
import br.com.automation.tests.pageobejcts.base.PageBase;

@Page
public class PageHome extends PageBase {
	@Autowired
	private WebDriver driver;
	
	@Autowired
	public PageHome(WebDriver driver) {
		super(driver);
	}
	
	// Xpath dinamico
	@FindBy(xpath = "//input[@class='%s']")
	private WebElement inputPesquisa;

	@FindBy(className = "nav-search-btn")
	private WebElement buttonPesquisa;

	public void setInputPesquisa(String keysToSend) {
		addDynamicElementKeys("inputPesquisa", "nav-search-input");
		inputPesquisa.sendKeys(keysToSend);
	}

	public void clickButtonPequisa() {
		buttonPesquisa.click();
	}
	
	public void openSite() {
		driver.get("https://www.mercadolivre.com.br/");
	}
}
