package br.com.automation.tests.steps.base;

import br.com.automation.configuration.spring.annotations.Step;
import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;

@Step
public class StepBrowser {
	@Dado("^que abri o browser$")
	public void queAbriOBrowser(DataTable arg1) throws Throwable {
		arg1.asList(String.class);
	}
}
