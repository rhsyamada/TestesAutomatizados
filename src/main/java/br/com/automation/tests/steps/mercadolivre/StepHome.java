package br.com.automation.tests.steps.mercadolivre;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.automation.configuration.spring.annotations.Step;
import br.com.automation.tests.functionalitys.mercadolivre.FuncHome;
import br.com.automation.tests.steps.base.StepBase;
import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

@Step
public class StepHome extends StepBase {
	@Autowired
	FuncHome home;

	@Dado("^que acessei o site do Mercado livre$")
	public void queAcesseiOSiteDoMercadoLivre() throws Throwable {
		home.abrirSite();
	}

	@Quando("^realizar a busca do produto no site do Mercado livre$")
	public void realizarABuscaDoProdutoNoSiteDoMercadoLivre(DataTable arg1) throws Throwable {
		for (Map<String, String> map : arg1.asMaps(String.class, String.class)) {
			home.preencherBusca(map.get("pesquisa"));
		}
	}

	@Entao("^validar que a busca foi realizada no site do Mercado livre$")
	public void validarQueABuscaFoiRealizadaNoSiteDoMercadoLivre() throws Throwable {
	}
}
