#language: pt
#Author: Ricardo Yamada
#Version: 1.0
#Encoding: iso-8859-1
Funcionalidade: Validar pesquisa Mercado livre

	@MercadoLivre
  Cenario: Validar pesquisa Mercado livre
    Dado que abri o browser
      | browser |
      | CRHOME  |
    E que acessei o site do Mercado livre
    Quando realizar a busca do produto no site do Mercado livre
      | pesquisa       |
      | CD da gretchen |
    Entao validar que a busca foi realizada no site do Mercado livre
